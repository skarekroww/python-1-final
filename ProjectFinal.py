# Joshua Mercer

def show_instructions():
    # print a main menu and the commands
    print("Resident Evil Text Adventure Game")
    print("Collect 6 items to win the game, or be eaten by the Crimson Head.")
    print("Move commands: 'go' South, 'go' North, 'go' East, 'go' West")
    print("Add items to your Inventory with: 'grab item name' \n ______________")


def main():
    current_room = 'Outside'

    directions = {

        'Main Room': {'North': 'Dinning Room', 'South': 'Green House', 'East': 'Basement', 'Item': 'Lockpick'},
        'Green House': {'North': 'Main Room', 'East': 'Shed', 'Item': 'Herb'},
        'Shed': {'West': 'Green House', 'Item': 'Gasoline'},
        'Dinning Room': {'South': 'Main Room', 'East': 'Kitchen', 'Item': 'Lighter'},
        'Kitchen': {'West': 'Dinning Room', 'Item': 'Knife'},
        'Basement': {'West': 'Main Room', 'North': 'Boss Room', 'Item': 'Key'},
        'Outside': {'East': 'Main Room'},
        'Boss Room': {'Boss Room'},
    }
    inventory = []

    while True:
        print("you're in", current_room)
        print("Inventory is:", inventory)
        print('you can head to', directions[current_room], '\n _______________')
        # get name
        player_command = input('Enter a movement command: ')
        # to find the first word said to see if exit, go, grab commands are given
        starting_Word = player_command.split()[0]
        # gets the last word of the sentence (so go east gets east)
        player_command = player_command.split()[-1].capitalize()

        if starting_Word == 'go':
            # if the player goes a valid direction moves to the room else error
            if player_command in directions[current_room]:
                # todo add detailed discretion of interacting with each room
                # to be determined on how to do so.
                current_room = directions[current_room][player_command]
            else:
                print('Invalid movement command.')
        # Grab command
        elif starting_Word == 'grab':
            if player_command == directions[current_room]['Item']:
                print('you grabbed the ' + directions[current_room]['Item'])
                inventory.append(directions[current_room]['Item'])
                # Removes ability to grab items
                directions[current_room].pop('Item')

            else:
                print('Sorry you can only grab the item.')
        # exit command
        elif starting_Word == 'exit':
            break
        # this checks to see if you win or lose
        if current_room == 'Boss Room' and len(inventory) == 6:
            print("After a length fight with the crimson head you manage to kill it. "
                  "After doing so you use the lighter and gas can to burn the body.")
            break
        elif current_room == 'Boss Room' and len(inventory) != 6:
            print("You get into a fight with the crimson head sadly you're unprepared and are eaten")
            break


show_instructions()
main()
